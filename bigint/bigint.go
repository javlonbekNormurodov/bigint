package bigint

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

type Bigint struct {
	Value string
}

var ErrorBadInput = errors.New("bad input, please input only number")

func NewInt(num string) (Bigint, error) {
	allowed := "1234567890"
	var err bool

	if strings.HasPrefix(num, "+") {
		num = strings.Replace(num, "+", "", 1)
	}
	if strings.HasPrefix(num, "0") {
		err = true
	}
	arr := strings.Split(num, "")
	for _, v := range arr {
		if !strings.Contains(allowed, v) {
			err = true
		}
	}

	if err {
		return Bigint{Value: num}, ErrorBadInput
	} else {
		return Bigint{Value: num}, nil

	}
}

func (z *Bigint) Set(num string) error {
	//
	fmt.Println("Add a number: ")
	fmt.Scan(&num)
	//
	z.Value = num
	return nil
}

func Add(a, b Bigint) Bigint {
	c,_ := strconv.ParseInt(a.Value,10,64)
	d,_ := strconv.ParseInt(b.Value,10,64) 
	sum := c + d
	return Bigint{strconv.FormatInt(int64(sum), 10)}
}

func Sub(a, b Bigint) Bigint {
		c,_ := strconv.ParseInt(a.Value,10,64)
		d,_ := strconv.ParseInt(b.Value,10,64)
		sum := c - d
	return Bigint{strconv.FormatInt(int64(sum),10)}
}

func Multiply(a, b Bigint) Bigint {
	c,_ := strconv.ParseInt(a.Value,10,64)
	d,_:= strconv.ParseInt(b.Value,10,64)
	sum := c * d
	return Bigint{strconv.FormatInt(int64(sum),10)}

}

func Mod(a, b Bigint) Bigint {
	c,_ := strconv.ParseInt(a.Value,10,64)
	d,_ := strconv.ParseInt(b.Value,10,64)
	sum := c % d
	return Bigint{strconv.FormatInt(int64(sum),10)}
}

func (x *Bigint) Abs() Bigint {
	if x.Value[0] == '-' {
		return Bigint{
			Value: x.Value[1:],
		}
	}
	if x.Value[0] == '+' {
		return Bigint{
			Value: x.Value[1:],
		}
	}
	return Bigint{
		Value: x.Value,
	}
}
