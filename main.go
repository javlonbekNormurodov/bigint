package main

import (
	"bootcamp/bigint/bigint"
	"fmt"
)

func main() {
	a, err := bigint.NewInt("10")
	if err != nil {
		panic(err)
	}
	b, err := bigint.NewInt("13")
	if err != nil {
		panic(err)
	}

	err = a.Set("1")
	if err != nil {
		panic(err)
    }
	c := bigint.Add(a, b)
	if err != nil{
		panic(err)
	}

	d := bigint.Sub(a, b)
	if err != nil{
		panic(err)
	}
	
	e := bigint.Multiply(a, b)
	if err != nil{
		panic(err)
	}
	f := bigint.Mod(a, b)
	fmt.Println(a)
	fmt.Println(b)
	fmt.Println(c)
	fmt.Println(d)
	fmt.Println(e)
	fmt.Println(f)
	
}
